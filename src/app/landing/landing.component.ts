import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})

export class LandingComponent implements OnInit {
  private userprofile: Array<any> = [];
  focus: any;
  focus1: any;

  constructor(private http: Http) { }

  ngOnInit() { }
  ngAfterViewInit(): void {
    this.loadData();
  }

  private loadData(): void {
    let self = this;
    this.http.request('http://54.37.98.101:8080/api/user-profiles')
      .subscribe((res: Response) => {
        self.userprofile = res.json();
        console.log(res);
      });
  }

}
